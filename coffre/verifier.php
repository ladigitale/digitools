<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['url']) && !empty($_POST['reponse'])) {
	require 'db.php';
	$url = $_POST['url'];
	$reponse = $_POST['reponse'];
	$stmt = $db->prepare('SELECT parametres FROM digitools WHERE url = :url');
	if ($stmt->execute(array('url' => $url))) {
		$resultat = $stmt->fetchAll();
		if ($resultat[0]['parametres'] && $resultat[0]['parametres'] !== '') {
			$parametres = $resultat[0]['parametres'];
			$parametres = json_decode(urldecode($parametres), true);
			if (trim(strtolower($parametres['combinaison'])) === trim(strtolower($reponse))) {
				$parametres = json_encode($parametres);
				$parametres = rawurlencode($parametres);
				echo $parametres;
			} else {
				echo 'combinaison_incorrecte';
			}
		} else {
			echo 'erreur';
		}
	}
	$db = null;
	exit();
} else {
	header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
}

?>
