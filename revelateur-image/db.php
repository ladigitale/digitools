<?php

if (!file_exists(dirname(__FILE__) . '/digitools_revelateur_image.db')) {
    $db = new PDO('sqlite:'. dirname(__FILE__) . '/digitools_revelateur_image.db');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $table = "CREATE TABLE digitools (
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        url TEXT NOT NULL,
        token TEXT NOT NULL,
        parametres TEXT NOT NULL,
        date TEXT NOT NULL,
        vues INTEGER NOT NULL,
        derniere_visite TEXT NOT NULL
    )";
    $db->exec($table);
} else {
    $db = new PDO('sqlite:'. dirname(__FILE__) . '/digitools_revelateur_image.db');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $db->prepare('PRAGMA table_info(digitools)');
	if ($stmt->execute()) {
        $tables = $stmt->fetchAll();
        if (count($tables) < 7) {
            $colonne = "ALTER TABLE digitools ADD vues INTEGER NOT NULL DEFAULT 0";
            $db->exec($colonne);
            $colonne = "ALTER TABLE digitools ADD derniere_visite TEXT NOT NULL DEFAULT ''";
            $db->exec($colonne);
        }
    }
}

?>
