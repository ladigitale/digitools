<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour afficher un chronomètre.">
        <title>Chronomètre - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Chronomètre</span>
		</header>
		<main>
			<div class="conteneur">
				<div id="chrono">
					<span id="minutes">00</span>
					<span class="separateur">:</span>
					<span id="secondes">00</span>
					<span class="separateur">.</span>
					<span id="millisecondes">000</span>
				</div>
				<div class="actions">
					<span id="demarrer" class="bouton" role="button" tabindex="0" onclick="demarrer()">Démarrer</span>
					<span id="continuer" class="bouton" role="button" tabindex="0" onclick="demarrer()">Continuer</span>
					<span id="suspendre" class="bouton" role="button" tabindex="0" onclick="suspendre()">Pause</span>
					<span id="reinitialiser" class="bouton" role="button" tabindex="0" onclick="reinitialiser()">Réinitialiser</span>
				</div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>

        <script>
			let chrono = null
			const texteMinutes = document.getElementById('minutes')
			let minutes = parseInt(texteMinutes.innerHTML)
			const texteSecondes = document.getElementById('secondes')
			let secondes = parseInt(texteSecondes.innerHTML)
			const texteMillisecondes = document.getElementById('millisecondes')
			let millisecondes = parseInt(texteMillisecondes.innerHTML)
			let millisecondesEcoulees = 0
			let millisecondesPause = 0

			document.querySelector('#demarrer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					demarrer()
				}
			})

			document.querySelector('#continuer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					demarrer()
				}
			})

			document.querySelector('#suspendre').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					suspendre()
				}
			})

			document.querySelector('#reinitialiser').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					reinitialiser()
				}
			})
			
			function arreterTempsMillisecondes (chrono) {
				if (chrono) { 
					clearInterval(chrono)
					return chrono
				}
				else return chrono
			}

			function demarrerTempsMillisecondes (ms) {
				const date = new Date()
				return date.getTime() - ms
			}

			function recupererTempsMillisecondes (ms) {
				if (ms > 0) {
					const date = new Date()
					return date.getTime() - ms
				} else {
					return 0
				}
			}
		
			function demarrer () { 
				chrono = arreterTempsMillisecondes(chrono)
				const ms = demarrerTempsMillisecondes(millisecondesPause)
				millisecondesPause = 0
				chrono = setInterval(function () {
					millisecondesEcoulees = recupererTempsMillisecondes(ms)
					if (millisecondes < 10) {
						texteMillisecondes.innerHTML = '00' + millisecondes
					} else if (millisecondes < 100) {
						texteMillisecondes.innerHTML = '0' + millisecondes
					} else {
						texteMillisecondes.innerHTML = millisecondes
					}
					if (secondes < 10) {
						texteSecondes.innerHTML = '0' + secondes
					} else {
						texteSecondes.innerHTML = secondes
					}
					if (minutes < 10) {
						texteMinutes.innerHTML = '0' + minutes
					} else {
						texteMinutes.innerHTML = minutes
					}
					millisecondes = millisecondesEcoulees
					if (minutes === 59 && secondes === 59 && millisecondes > 999) {
						secondes = 0
						minutes = 0
					}
					if (secondes === 59 && millisecondes > 999) {
						secondes = 0
						minutes++
					}
					if (millisecondes > 999) {
						millisecondes = 0
						secondes++;
						demarrer()
					}
				}, 1)
				document.querySelector('#suspendre').style.display = 'inline-block'
				document.querySelector('#reinitialiser').style.display = 'inline-block'
				document.querySelector('#demarrer').style.display = 'none'
				document.querySelector('#continuer').style.display = 'none'
			}

			function suspendre () {
				millisecondesPause = millisecondesEcoulees
				chrono = arreterTempsMillisecondes(chrono)
				document.querySelector('#continuer').style.display = 'inline-block'
				document.querySelector('#demarrer').style.display = 'none'
				document.querySelector('#suspendre').style.display = 'none'
				return true
			}

			function reinitialiser () {
				chrono = arreterTempsMillisecondes(chrono)
				texteMillisecondes.innerHTML = '000'
				millisecondes = 0
				texteSecondes.innerHTML = '00'
				secondes = 0
				texteMinutes.innerHTML = '00'
				minutes = 0
				document.querySelector('#demarrer').style.display = 'inline-block'
				document.querySelector('#suspendre').style.display = 'none'
				document.querySelector('#continuer').style.display = 'none'
				document.querySelector('#reinitialiser').style.display = 'none'
				return true
			}

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				document.querySelector('.conteneur').style.padding = '50px 20px'
			}
        </script>
    </body>
</html>
