<?php

$url = '';
$parametres = '';
$token = '';
$admin = 0;
$vues = 0;

if (!empty($_GET['p'])) {
	require 'db.php';
	if (!empty($_GET['t'])) {
		$token = $_GET['t'];
	}
	$url = $_GET['p'];
	$stmt = $db->prepare('SELECT token, parametres, vues FROM digitools WHERE url = :url');
	if ($stmt->execute(array('url' => $url))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			$db = null;
			return false;
		}
		if ($resultat[0]['parametres'] && $resultat[0]['parametres'] !== '') {
			$parametres = rawurlencode($resultat[0]['parametres']);
		} else {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] === $token) {
			$admin = 1;
		} else if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] !== $token) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		$date = date('Y-m-d H:i:s');
		if ($resultat[0]['vues'] !== '') {
			$vues = intval($resultat[0]['vues']);
		}
		if ($admin === 0) {
			$vues = $vues + 1;
		}
		$stmt = $db->prepare('UPDATE digitools SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
		$stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $url));
	}
	$db = null;
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour créer des groupes.">
        <title>Groupes - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Groupes</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Indiquez dans le champs de texte ci-dessous les noms des personnes à placer dans les groupes, séparés par une virgule. <u>Exemple</u>&nbsp;: Jeanne, Albert, Gaston, Gaëlle.</p>
				<p>Précisez ensuite le nombre de personnes par groupe.</p>
			</div>
			<div class="conteneur conteneur-edition">
				<div id="champ1">
					<label for="liste-personnes">Liste des personnes</label>
					<textarea id="liste-personnes"></textarea>
				</div>
				<div id="champ2">
					<label for="nombre-personnes">Nombre de personnes par groupe</label>
					<input id="nombre-personnes" type="number" value="0">
				</div>
				<div class="actions">
					<span id="recommencer" class="bouton" role="button" tabindex="0" onclick="recommencer()">Recommencer</span>
					<span id="creer" class="bouton" role="button" tabindex="0" onclick="creer()">Valider</span>
				</div>
			</div>
			<div class="conteneur conteneur-groupes">
				<div id="groupes"></div>
				<div class="actions">
					<span id="generer" class="bouton" role="button" tabindex="0" onclick="generer()">Générer</span>
					<span id="quitter" class="bouton" role="button" tabindex="0" onclick="quitter()">Quitter</span>
				</div>
			</div>
		</main>
		<footer>
			<?php if ($vues > 1) { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie. - <?php echo $vues; ?> vues</p>
			<?php } else { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<?php } ?>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="../clipboard.js"></script>
        <script>
			let liste = []
			let personnes = 0
			let url = ''
			let token = ''
			const hote = window.location.href.split('?')[0]

			document.querySelector('#recommencer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					recommencer()
				}
			})

			document.querySelector('#creer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					creer()
				}
			})

			document.querySelector('#generer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					generer()
				}
			})

			document.querySelector('#quitter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					quitter()
				}
			})
			
			function creer (action) {
				if (action === 'lire') {
					if (liste.length > 0 && personnes > 0) {
						document.querySelector('.conteneur-groupes').style.display = 'block'
						document.querySelector('.conteneur-consigne').style.display = 'none'
						document.querySelector('.conteneur-edition').style.display = 'none'
						generer()
					}
				} else {
					liste = document.querySelector('#champ1 textarea').value
					liste = liste.split(',')
					personnes = parseInt(document.querySelector('#champ2 input').value)
					if (liste.length > 0 && personnes > 0) {
						const xhr = new XMLHttpRequest()
						xhr.onload = function () {
							if (xhr.readyState === xhr.DONE && xhr.status === 200) {
								if (xhr.responseText !== 'erreur') {
									const donnees = JSON.parse(xhr.responseText)
									url = donnees.url
									token = donnees.token
									window.location = hote + '?p=' + url + '&t=' + token
								} else {
									alert('Erreur de communication avec le serveur.')
								}
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						}
						xhr.open('POST', hote + 'enregistrer.php', true)
						xhr.setRequestHeader('Content-type', 'application/json')
						xhr.send(JSON.stringify({ url: url, token: token, parametres: JSON.stringify({ liste: liste, personnes: personnes }) }))
					}
				}
            }
			
			function generer () {
				liste = melanger(liste)
				let nombreGroupes = Math.ceil(liste.length / personnes)
				const groupes = []
				let indexPersonne = 0
				while (nombreGroupes > 0) {
					const groupe = []
					let i
					if (nombreGroupes < 1) {
						while (indexPersonne < liste.length) {
							groupe[groupe.length] = liste[indexPersonne]
							indexPersonne++
						}
					} else {
						for (i = personnes; i > 0 ; i--) {
							groupe[groupe.length] = liste[indexPersonne]
							indexPersonne++
						}
					}
					groupes[groupes.length] = groupe
					nombreGroupes--
				}
				let html = ''
				groupes.forEach(function (groupe, index) {
					html += '<div class="groupe"><div class="titre">Groupe ' + (index + 1) + '</div><ul>'
					groupe.forEach(function (personne) {
						if (personne && personne !== '') {
							html += '<li>' + personne.trim() + '</li>'
						}
					})
					html += '</ul></div>'
				})
				document.querySelector('#groupes').innerHTML = html
			}

            function quitter () {
				document.querySelector('#creer').textContent = 'Modifier'
				document.querySelector('.conteneur-consigne').style.display = 'block'
				document.querySelector('.conteneur-edition').style.display = 'block'
				document.querySelector('.conteneur-groupes').style.display = 'none'
            }
			
			function recommencer () {
				window.location = hote
			}
			
			function genererLiens (url, token) {
				const lienAdmin = hote + '?p=' + url + '&t=' + token
				const lien = hote + '?p=' + url
				const element = document.createElement('div')
				element.id = 'liens'
				element.innerHTML = '<label>Lien d\'administration</label><input type="text" id="lien-admin" value="' + lienAdmin + '" disabled><span id="copier-admin" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><label>Lien de partage</label><input type="text" id="lien" value="' + lien + '" disabled><span id="copier" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><div id="supprimer"><span class="bouton" role="button" tabindex="0" onclick="supprimer()">Supprimer</span></div>'
				document.querySelector('footer').insertAdjacentElement('beforebegin', element)

				document.querySelector('#copier-admin').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier-admin').click()
					}
				})

				document.querySelector('#copier').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier').click()
					}
				})

				const clipboardAdmin = new ClipboardJS('#copier-admin', {
					text: function () {
						return lienAdmin
					}
				})
				clipboardAdmin.on('success', function () {
					document.querySelector('#copier-admin').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien d\'administration copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
				const clipboardLien = new ClipboardJS('#copier', {
					text: function () {
						return lien
					}
				})
				clipboardLien.on('success', function () {
					document.querySelector('#copier').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien de partage copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
			}

			function supprimer () {
				if (confirm('Souhaitez-vous vraiment supprimer ce contenu ?')) {
					const xhr = new XMLHttpRequest()
					xhr.onload = function () {
						if (xhr.readyState === xhr.DONE && xhr.status === 200) {
							if (xhr.responseText !== 'erreur') {
								window.location = hote
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						} else {
							alert('Erreur de communication avec le serveur.')
						}
					}
					xhr.open('POST', hote + 'supprimer.php', true)
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
					xhr.send('url=' + url + '&token=' + token)
				} 
			}
			
			function melanger (array) {
				for (let i = array.length - 1; i > 0; i--) {
					const j = Math.floor(Math.random() * (i + 1))
					const temp = array[i]
					array[i] = array[j]
					array[j] = temp
				}
				return array
			}
			
			window.addEventListener('load', function () {
				if ('<?php echo $url; ?>' !== '' && '<?php echo $parametres; ?>' !== '') {
					url = '<?php echo $url; ?>'
					const donnees = JSON.parse(decodeURIComponent('<?php echo $parametres; ?>'))
					liste = donnees.liste
					personnes = donnees.personnes
					document.querySelector('#champ1 textarea').value = liste.toString()
					document.querySelector('#champ2 input').value = personnes
					creer('lire')
				} else {
					document.querySelector('#recommencer').style.display = 'none'
				}
				if (parseInt('<?php echo $admin; ?>') === 1) {
					token = '<?php echo $token; ?>'
					if (window === window.parent) {
						genererLiens(url, token)
					}
				} else {
					document.querySelector('#quitter').parentNode.removeChild(document.querySelector('#quitter'))
				}
				document.body.style.display = 'block'
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
			}
        </script>
    </body>
</html>
