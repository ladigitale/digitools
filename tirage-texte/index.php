<?php

$url = '';
$parametres = '';
$token = '';
$admin = 0;
$vues = 0;

if (!empty($_GET['p'])) {
	require 'db.php';
	if (!empty($_GET['t'])) {
		$token = $_GET['t'];
	}
	$url = $_GET['p'];
	$stmt = $db->prepare('SELECT token, parametres, vues FROM digitools WHERE url = :url');
	if ($stmt->execute(array('url' => $url))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			$db = null;
			return false;
		}
		if ($resultat[0]['parametres'] && $resultat[0]['parametres'] !== '') {
			$parametres = rawurlencode($resultat[0]['parametres']);
		} else {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] === $token) {
			$admin = 1;
		} else if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] !== $token) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		$date = date('Y-m-d H:i:s');
		if ($resultat[0]['vues'] !== '') {
			$vues = intval($resultat[0]['vues']);
		}
		if ($admin === 0) {
			$vues = $vues + 1;
		}
		$stmt = $db->prepare('UPDATE digitools SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
		$stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $url));
	}
	$db = null;
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour tirer au sort du texte.">
        <title>Tirage au sort (texte) - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Tirage au sort (texte)</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Indiquez dans le champ de texte ci-dessous les mots ou phrases à tirer au sort, séparés par une virgule. <u>Exemple</u>&nbsp;: Bonjour, Au revoir, Salut.</p>
			</div>
			<div class="conteneur conteneur-edition">
				<div id="champs">
					<div id="champ">
						<label for="liste">Liste de mots ou de phrases</label>
						<textarea id="liste"></textarea>
					</div>
					<div class="actions">
						<span id="ajouter" class="bouton" role="button" tabindex="0" onclick="ajouter()">Ajouter</span>
					</div>
				</div>
				<div id="items"></div>
				<div id="options">
					<label>Supprimer l'item de la liste après le tirage&nbsp;?</label>
					<div id="choix">
						<span class="oui">
							<input type="radio" id="oui" name="suppression" value="oui">
							<label for="oui" tabindex="0" onkeydown="activerInput(event, 'oui')">Oui</label>
						</span>
						<span class="non">
							<input type="radio" id="non" name="suppression" value="non" checked>
							<label for="non" tabindex="0" onkeydown="activerInput(event, 'non')">Non</label>
						</span>
					</div>
				</div>
				<div class="actions">
					<span id="recommencer" class="bouton" role="button" tabindex="0" onclick="recommencer()">Recommencer</span>
					<span id="creer" class="bouton" role="button" tabindex="0" onclick="creer('enregistrer')">Valider</span>
				</div>
			</div>
			<div class="conteneur conteneur-creation">
				<div id="texte"></div>
				<div class="actions">
					<span id="generer" class="bouton" role="button" tabindex="0" onclick="generer()">Choisir</span>
					<span id="quitter" class="bouton" role="button" tabindex="0" onclick="quitter()">Quitter</span>
				</div>
			</div>
		</main>
		<footer>
			<?php if ($vues > 1) { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie. - <?php echo $vues; ?> vues</p>
			<?php } else { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<?php } ?>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="../clipboard.js"></script>
        <script>
			let liste = []
			let copieListe = []
			let suppression = 'non'
			let tirage = ''
			let url = ''
			let token = ''
			const hote = window.location.href.split('?')[0]

			document.querySelector('#ajouter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ajouter()
				}
			})

			document.querySelector('#recommencer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					recommencer()
				}
			})

			document.querySelector('#creer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					creer('enregistrer')
				}
			})

			document.querySelector('#generer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					generer()
				}
			})

			document.querySelector('#quitter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					quitter()
				}
			})

			function activerInput (event, id) {
				if (event.key === 'Enter') {
					document.querySelector('#' + id).click()
				}
			}
			
			function ajouter () {
				let items = document.querySelector('#champ textarea').value
				if (items !== '') {
					items = items.split(',').map(item => item.trim())
					items = items.filter((item, i, ar) => ar.indexOf(item) === i)
					items.forEach(function (item) {
						if (liste.includes(item) === false) {
							const div = document.createElement('div')
							div.innerHTML = '<span role="button" tabindex="0" data-item="' + item + '"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="24px" height="24px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1024 1024"><path d="M685.4 354.8c0-4.4-3.6-8-8-8l-66 .3L512 465.6l-99.3-118.4l-66.1-.3c-4.4 0-8 3.5-8 8c0 1.9.7 3.7 1.9 5.2l130.1 155L340.5 670a8.32 8.32 0 0 0-1.9 5.2c0 4.4 3.6 8 8 8l66.1-.3L512 564.4l99.3 118.4l66 .3c4.4 0 8-3.5 8-8c0-1.9-.7-3.7-1.9-5.2L553.5 515l130.1-155c1.2-1.4 1.8-3.3 1.8-5.2z" fill="#626262"/><path d="M512 65C264.6 65 64 265.6 64 513s200.6 448 448 448s448-200.6 448-448S759.4 65 512 65zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372s372 166.6 372 372s-166.6 372-372 372z" fill="#626262"/><rect x="0" y="0" width="1024" height="1024" fill="rgba(0, 0, 0, 0)" /></svg></span>' + item
							document.querySelector('#items').appendChild(div)
							liste.push(item)
						}
					})
					const elements = document.querySelectorAll('#items div')
					elements.forEach(function (element) {
						const dataItem = element.querySelector('span').getAttribute('data-item')
						element.querySelector('span').addEventListener('click', function () {
							liste.forEach(function (item, index) {
								if (item === dataItem) {
									liste.splice(index, 1)
									element.parentNode.removeChild(element)
								}
							})
						})

						element.querySelector('span').addEventListener('keydown', function (e) {
							if (e.key === 'Enter') {
								liste.forEach(function (item, index) {
									if (item === dataItem) {
										liste.splice(index, 1)
										element.parentNode.removeChild(element)
									}
								})
							}
						})
					})
					document.querySelector('#champ textarea').value = ''
				}
            }
			
			function creer (action) {
				if (action === 'lire' && liste.length > 0) {
					document.querySelector('.conteneur-creation').style.display = 'block'
					document.querySelector('.conteneur-consigne').style.display = 'none'
					document.querySelector('.conteneur-edition').style.display = 'none'
					document.querySelector('#generer').style.display = 'inline-block'
					document.querySelector('#texte').textContent = '✨✨✨'
					copieListe = JSON.parse(JSON.stringify(liste))
				} else if (liste.length > 0) {
					if (document.querySelector('#oui').checked === true) {
						suppression = 'oui'
					} else {
						suppression = 'non'
					}
					const xhr = new XMLHttpRequest()
					xhr.onload = function () {
						if (xhr.readyState === xhr.DONE && xhr.status === 200) {
							if (xhr.responseText !== 'erreur') {
								const donnees = JSON.parse(xhr.responseText)
								url = donnees.url
								token = donnees.token
								window.location = hote + '?p=' + url + '&t=' + token
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						} else {
							alert('Erreur de communication avec le serveur.')
						}
					}
					xhr.open('POST', hote + 'enregistrer.php', true)
					xhr.setRequestHeader('Content-type', 'application/json')
					xhr.send(JSON.stringify({ url: url, token: token, parametres: JSON.stringify({ liste: JSON.stringify(liste), suppression: suppression }) }))
				}
            }

            function generer () {
				if (suppression === 'oui' && copieListe.length > 1) {
					copieListe.forEach(function (item, index) {
						if (item === tirage) {
							copieListe.splice(index, 1)
						}
					})
				}
				document.querySelector('#texte').classList.add('tirage')
				const tirageEnCours = setInterval(function () {
					const texte = copieListe[Math.floor(Math.random() * copieListe.length)]
					tirage = texte
					document.querySelector('#texte').textContent = texte.trim()
				}, 10)
				setTimeout(function () {
					clearInterval(tirageEnCours)
					document.querySelector('#texte').classList.remove('tirage')
				}, 1200)
				if (copieListe.length === 1) {
					document.querySelector('#generer').style.display = 'none'
				}
            }

            function quitter () {
				const items = liste.toString()
				document.querySelector('#champ textarea').value = items
				liste = []
				tirage = ''
				ajouter()
				if (suppression === 'oui') {
					document.querySelector('#oui').checked = true
				} else {
					document.querySelector('#non').checked = true
				}
				document.querySelector('#creer').textContent = 'Modifier'
				document.querySelector('.conteneur-consigne').style.display = 'block'
				document.querySelector('.conteneur-edition').style.display = 'block'
				document.querySelector('#recommencer').style.display = 'inline-block'
				document.querySelector('.conteneur-creation').style.display = 'none'
            }
			
			function recommencer () {
				window.location = hote
			}
			
			function genererLiens (url, token) {
				const lienAdmin = hote + '?p=' + url + '&t=' + token
				const lien = hote + '?p=' + url
				const element = document.createElement('div')
				element.id = 'liens'
				element.innerHTML = '<label>Lien d\'administration</label><input type="text" id="lien-admin" value="' + lienAdmin + '" disabled><span id="copier-admin" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><label>Lien de partage</label><input type="text" id="lien" value="' + lien + '" disabled><span id="copier" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><div id="supprimer"><span class="bouton" role="button" tabindex="0" onclick="supprimer()">Supprimer</span></div>'
				document.querySelector('footer').insertAdjacentElement('beforebegin', element)

				document.querySelector('#copier-admin').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier-admin').click()
					}
				})

				document.querySelector('#copier').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier').click()
					}
				})

				const clipboardAdmin = new ClipboardJS('#copier-admin', {
					text: function () {
						return lienAdmin
					}
				})
				clipboardAdmin.on('success', function () {
					document.querySelector('#copier-admin').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien d\'administration copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
				const clipboardLien = new ClipboardJS('#copier', {
					text: function () {
						return lien
					}
				})
				clipboardLien.on('success', function () {
					document.querySelector('#copier').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien de partage copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
			}

			function supprimer () {
				if (confirm('Souhaitez-vous vraiment supprimer ce contenu ?')) {
					const xhr = new XMLHttpRequest()
					xhr.onload = function () {
						if (xhr.readyState === xhr.DONE && xhr.status === 200) {
							if (xhr.responseText !== 'erreur') {
								window.location = hote
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						} else {
							alert('Erreur de communication avec le serveur.')
						}
					}
					xhr.open('POST', hote + 'supprimer.php', true)
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
					xhr.send('url=' + url + '&token=' + token)
				} 
			}
			
			window.addEventListener('load', function () {
				if ('<?php echo $url; ?>' !== '' && '<?php echo $parametres; ?>' !== '') {
					url = '<?php echo $url; ?>'
					const donnees = JSON.parse(decodeURIComponent('<?php echo $parametres; ?>'))
					if (donnees.hasOwnProperty('suppression')) {
						liste = JSON.parse(donnees.liste)
						suppression = donnees.suppression
					} else {
						liste = donnees
					}
					creer('lire')
				} else {
					document.querySelector('#recommencer').style.display = 'none'
				}
				if (parseInt('<?php echo $admin; ?>') === 1) {
					token = '<?php echo $token; ?>'
					if (window === window.parent) {
						genererLiens(url, token)
					}
				} else {
					document.querySelector('#quitter').parentNode.removeChild(document.querySelector('#quitter'))
				}
				document.body.style.display = 'block'
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				if (document.querySelector('#texte')) {
					document.querySelector('#texte').style.padding = '30px 0'
				}
			}
        </script>
    </body>
</html>
