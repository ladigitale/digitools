<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour générer des histoires à partir d'images.">
        <title>Générateur d'histoire - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Générateur d'histoire</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Générez ci-dessous une trame d'histoire à partir du nombre d'images défini.</p>
				<p class="petit">Les symboles pictographiques utilisés sont la propriété du Gouvernement d'Aragon et ont été créés par Sergio Palao pour ARASAAC (<a href="https://arasaac.org" target="_blank">https://arasaac.org)</a>, qui les distribuent sous Licence Creative Commons BY-NC-SA.</p>
			</div>
			<div class="conteneur conteneur-edition">
				<div id="champ">
					<label for="images">Nombre d'images</label>
					<select id="images">
						<option value="3">3</option>
						<option value="5" selected>5</option>
						<option value="7">7</option>
						<option value="9">9</option>
					</select>
				</div>
				<div class="actions">
					<span id="generer" class="bouton" role="button" tabindex="0" onclick="generer()">Générer</span>
				</div>
				<div id="histoire"></div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="../clipboard.js"></script>
        <script>
			const images = ['aeroport.png', 'amour.png', 'ampoule.png', 'anniversaire.png', 'appareil-photo.png', 'argent.png', 'autobus.png', 'automne.png', 'avalanche.png', 'baleine.png', 'ballon-air.png', 'ballon.png', 'banque.png', 'bar.png', 'bar-restaurant.png', 'basket-fauteuil.png', 'bateau-pirate.png', 'bateau.png', 'bibliotheque.png', 'bicyclette.png', 'big-ben.png', 'bijoux.png', 'boite.png', 'bouee-sauvetage.png', 'boulangerie.png', 'bouteille.png', 'boutique-de-cadeaux.png', 'buvette.png', 'cafe.png', 'camion-de-pompiers.png', 'camping-car.png', 'canard-plastique.png', 'canard.png', 'capitole.png', 'carafe.png', 'carte.png', 'cartes.png', 'caserne-de-pompiers.png', 'casque-de-protection.png', 'cassette-video.png', 'chaise.png', 'champignons.png', 'chapeau-melon.png', 'chapiteau.png', 'charcuterie.png', 'chateau.png', 'chat.png', 'chaussures-de-montagne.png', 'chaussures-de-sport.png', 'cheval.png', 'chien.png', 'chocolat.png', 'cinema.png', 'ciseaux.png', 'cles.png', 'coccinelle.png', 'cocotier.png', 'coffre-fort.png', 'coffre.png', 'colisee.png', 'commissariat.png', 'console.png', 'coquillages.png', 'courses.png', 'couverts.png', 'crevaison.png', 'crocodile.png', 'cygne.png', 'danseurs.png', 'dinosaure-peluche.png', 'diplôme.png', 'disque.png', 'donut.png', 'dromadaire.png', 'eau-gazeuse.png', 'eau.png', 'echiquier.png', 'ecureuil.png', 'elephant.png', 'eolienne.png', 'eruption-volcanique.png', 'essaim-abeilles.png', 'ete.png', 'etoile-de-mer.png', 'fer-cheval.png', 'fete.png', 'feu.png', 'flan-caramel.png', 'fleuve.png', 'foret.png', 'fromage.png', 'fruiterie.png', 'fruits-de-mer.png', 'fruits.png', 'fusee.png', 'galerie-d-art.png', 'gateau-creme.png', 'guitare.png', 'hamburger-frites.png', 'hiver.png', 'hopital.png', 'hotel.png', 'igloo.png', 'jo.png', 'jumelles.png', 'jus-de-pomme.png', 'jus-d-orange.png', 'kangourou.png', 'kayak.png', 'lait-amande.png', 'lait.png', 'lampe-de-poche.png', 'lampe.png', 'legumes.png', 'lentilles.png', 'librairie.png', 'lion.png', 'lit.png', 'longue-vue.png', 'loup.png', 'machine-a-laver.png', 'magasin-de-bijoux.png', 'magasin-de-bonbons.png', 'magasin-de-fleurs.png', 'magasin-de-glaces.png', 'magasin-de-sport.png', 'magasin-de-vetements.png', 'maison.png', 'manette.png', 'masques.png', 'mate.png', 'matin.png', 'medaille-argent.png', 'medaille-bronze.png', 'medaille-or.png', 'metro.png', 'monospace.png', 'montagne.png', 'montre.png', 'moto.png', 'muraille-chine.png', 'musique.png', 'noel.png', 'nuage.png', 'nuit.png', 'oeufs-paques.png', 'oeufs.png', 'oie.png', 'opera-sydney.png', 'orage.png', 'ordinateur.png', 'ordinateur-portable.png', 'ours.png', 'ovni.png', 'palmier.png', 'parapluie.png', 'parasol.png', 'parc.png', 'parfumerie.png', 'pates.png', 'patins.png', 'pendule.png', 'phare.png', 'pharmacie.png', 'pile.png', 'pingouin.png', 'piscine.png', 'pizzeria.png', 'pluie.png', 'podium.png', 'poisson.png', 'pomme-de-pin.png', 'portable.png', 'porte-de-brandebourg.png', 'poubelle.png', 'poulet.png', 'printemps.png', 'puzzle.png', 'quilles.png', 'refrigerateur.png', 'reveil-matin.png', 'rose.png', 'sablier.png', 'sac-sport.png', 'sagrada-familia.png', 'salon-de-coiffure.png', 'sandwich.png', 'sapin.png', 'scooter.png', 'scorpion.png', 'serpent.png', 'serviette-de-bain.png', 'sifflet.png', 'soja.png', 'soleil.png', 'sphynx.png', 'sport.png', 'statue-de-la-liberte.png', 'supermarche.png', 'table-de-pique-nique.png', 'tablette.png', 'taj-mahal.png', 'talkies-walkies.png', 'tarentule.png', 'tarte.png', 'taxi.png', 'tempete.png', 'tente.png', 'the-glace.png', 'the.png', 'tigre.png', 'tong.png', 'tonneau.png', 'torche.png', 'tornade.png', 'tortue.png', 'tour-eiffel.png', 'train.png', 'trottinette.png', 'tv.png', 'vache.png', 'valise.png', 'viande.png', 'village.png', 'ville.png', 'vin.png', 'voiture-de-sport.png', 'voiture.png']
			let nombre = 5

			document.querySelector('#generer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					generer()
				}
			})
			
			function generer () {
				nombre = parseInt(document.querySelector('#champ select').value)
				const selection = []
				for (let i = 0; i < nombre;) {
					const image = Math.floor(Math.random() * images.length)
					if (selection.includes(images[image]) === true) {
						continue
					}
					selection.push(images[image])
					i++
				}
				let html = ''
				selection.forEach(function (image) {
					html += '<img src="./images/' + image + '" alt="' + image + '">'
				})
				document.querySelector('#histoire').style.display = 'flex'
				document.querySelector('#histoire').innerHTML = html
            }

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
			}
        </script>
    </body>
</html>
