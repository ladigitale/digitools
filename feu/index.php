<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour afficher un feu tricolore.">
        <title>Feu tricolore - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1700507935769" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Feu tricolore</span>
		</header>
		<main>
			<div class="conteneur">
				<div id="feu">
					<div class="feu rouge" role="button" tabindex="0"></div>
					<div class="feu jaune" role="button" tabindex="0"></div>
					<div class="feu vert" role="button" tabindex="0"></div>
				</div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
        <script>
			const feux = document.querySelectorAll('.feu')
			feux.forEach(function (feu) {
				feu.addEventListener('click', function (event) {
					feux.forEach(function (element) {
						element.classList.remove('allume')
					})
					event.target.classList.add('allume')
				})

				feu.addEventListener('keydown', function (event) {
					if (event.key === 'Enter') {
						feux.forEach(function (element) {
							element.classList.remove('allume')
						})
						event.target.classList.add('allume')
					}
				})
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				document.querySelector('.conteneur').style.padding = '30px 20px'
				document.querySelector('.actions').style.marginTop = '30px'
			}
        </script>
    </body>
</html>
