<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour afficher un compte à rebours.">
        <title>Compte à rebours - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Compte à rebours</span>
		</header>
		<main>
			<div class="conteneur conteneur-edition">
				<div id="champs">
					<div id="champ1">
						<label for="champ-minutes">Minutes</label>
						<input id="champ-minutes" type="number" value="0" min="0">
					</div>
					<div id="champ2">
						<label for="champ-secondes">Secondes</label>
						<input id="champ-secondes" type="number" value="0" min="0" max="59">
					</div>
				</div>
				<div class="actions">
					<span id="demarrer" class="bouton" role="button" tabindex="0" onclick="demarrer(0)">Démarrer</span>
				</div>
			</div>
			<div class="conteneur conteneur-decompte">
				<div id="rebours">
					<svg width="300" height="300">
						<circle cx="150" cy="150" r="142" stroke="#00ced1" stroke-width="16" fill="#f9f9f9" />
					</svg>
					<div id="decompte">
						<span id="minutes">00</span>
						<span class="separateur">:</span>
						<span id="secondes">00</span>
					</div>
				</div>
				<div class="actions">
					<span id="continuer" class="bouton" role="button" tabindex="0" onclick="continuer()">Continuer</span>
					<span id="suspendre" class="bouton" role="button" tabindex="0" onclick="suspendre()">Pause</span>
					<span id="quitter" class="bouton" role="button" tabindex="0" onclick="quitter()">Quitter</span>
				</div>
			</div>
			<audio id="bip" preload="auto"><source src="./bip.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="fin" preload="auto"><source src="./fin.mp3" type="audio/mpeg" style="display: none;"></audio>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>

        <script>
			let decompte
			let duree = 0
			let pause = false
			let tempsRestant = 0
			const texteMinutes = document.querySelector('#minutes')
			const texteSecondes = document.querySelector('#secondes')

			document.querySelector('#demarrer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					demarrer(0)
				}
			})

			document.querySelector('#continuer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					continuer()
				}
			})

			document.querySelector('#suspendre').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					suspendre()
				}
			})

			document.querySelector('#quitter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					quitter()
				}
			})

			function calculerTempsRestant (d) {
				const temps = Date.parse(d) - Date.parse(new Date())
				const secondes = Math.floor((temps / 1000) % 60 )
				const minutes = Math.floor((temps / 1000 / 60) % 60 )
				return { 'total': temps, 'minutes': minutes, 'secondes': secondes }
			}

			function demarrer (laps) {
				if (parseInt(document.querySelector('#champ1 input').value) >= 1 || parseInt(document.querySelector('#champ2 input').value) >= 1) {
					if (laps === 0) {
						const maintenant = Date.parse(new Date())
						const minutes = parseInt(document.querySelector('#champ1 input').value)
						const secondes = parseInt(document.querySelector('#champ2 input').value)
						duree = new Date(maintenant + (((minutes * 60) + secondes) * 1000))
						const dureeMs = ((minutes * 60) + secondes) * 1000
						document.querySelector('#rebours svg').classList.add('actif')
						document.querySelector('#rebours svg').style.animation = 'rebours ' + dureeMs + 'ms linear forwards'
						document.querySelector('#rebours svg').style.strokeDasharray = Math.PI * 2 * 142
						document.querySelector('#rebours svg').style.strokeDashoffset =  Math.PI * 2 * 142
					} else {
						duree = laps
						document.querySelector('#rebours svg').classList.add('actif')
					}
					function decompter () {
						const temps = calculerTempsRestant(duree)
						if (temps.secondes < 10) {
							texteSecondes.innerHTML = '0' + temps.secondes
						} else {
							texteSecondes.innerHTML = temps.secondes
						}
						if (temps.minutes < 10) {
							texteMinutes.innerHTML = '0' + temps.minutes
						} else {
							texteMinutes.innerHTML = temps.minutes
						}
						if (temps.total <= 10000) {
							document.querySelector('#rebours svg').classList.add('rouge')
						}
						if (temps.total <= 10000 && temps.total > 0) {
							document.querySelector('#bip').play()
						}
						if (temps.total <= 0) {
							clearInterval(decompte)
							document.querySelector('#fin').play()
							document.querySelector('#suspendre').style.display = 'none'
						}
					}
					decompter()
					decompte = setInterval(decompter, 1000)
					document.querySelector('.conteneur-decompte').style.display = 'block'
					document.querySelector('#suspendre').style.display = 'inline-block'
					document.querySelector('#quitter').style.display = 'inline-block'
					document.querySelector('.conteneur-edition').style.display = 'none'
					document.querySelector('#demarrer').style.display = 'none'
					document.querySelector('#continuer').style.display = 'none'
				}
			}

			function suspendre () {
				if (!pause) {
					pause = true
					clearInterval(decompte)
					tempsRestant = calculerTempsRestant(duree).total
					document.querySelector('#continuer').style.display = 'inline-block'
					document.querySelector('#suspendre').style.display = 'none'
					document.querySelector('#rebours svg').classList.remove('actif')
				}
			}

			function continuer () {
				if (pause) {
					pause = false
					duree = new Date(Date.parse(new Date()) + tempsRestant)
					demarrer(duree)
					document.querySelector('#suspendre').style.display = 'inline-block'
					document.querySelector('#continuer').style.display = 'none'
				}
			}
			
			function quitter () {
				duree = 0
				pause = false
				tempsRestant = 0
				clearInterval(decompte)
				document.querySelector('#demarrer').style.display = 'inline-block'
				document.querySelector('.conteneur-decompte').style.display = 'none'
				document.querySelector('#suspendre').style.display = 'none'
				document.querySelector('#quitter').style.display = 'none'
				document.querySelector('.conteneur-edition').style.display = 'block'
				document.querySelector('#continuer').style.display = 'none'
				document.querySelector('#rebours svg').removeAttribute('style')
				document.querySelector('#rebours svg').classList.remove('rouge')
			}

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				document.querySelector('.conteneur-decompte').style.padding = '50px 20px'
			}
        </script>
    </body>
</html>
