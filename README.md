# Digitools

Digitools est un ensemble d'outils simples pour accompagner l'animation de cours en présence ou à distance.

Il est publié sous licence GNU AGPLv3. Sauf la fonte Orbitron (Apache License Version 2.0) et la fonte Mona Sans Expanded (Sil Open Font Licence 1.1)

Les symboles pictographiques utilisés dans le module Générateur d'histoire sont la propriété du Gouvernement d'Aragon et ont été créés par Sergio Palao pour ARASAAC (https://arasaac.org), qui les distribuent sous Licence Creative Commons BY-NC-SA.

### Serveur PHP nécessaire avec extension SQLite
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Démo
https://ladigitale.dev/digitools/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

