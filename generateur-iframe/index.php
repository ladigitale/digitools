<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour générer des codes d'intégration iframe.">
        <title>Générateur d'iframe - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Générateur d'iframe</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Indiquez le lien de l'iframe à générer puis précisez les options de la balise. Prévisualisez ensuite le résultat avant de copier le code d'intégration.</p>
			</div>
			<div class="conteneur conteneur-edition">
				<div id="champs">
					<div class="champs">
						<div id="champ1">
							<label for="champ-lien">Lien</label>
							<input id="champ-lien" type="text">
						</div>
					</div>
					<div class="champs">
						<div id="champ2">
							<label for="champ-nom">Nom</label>
							<input id="champ-nom" type="text" value="Mon iframe">
						</div>
					</div>
					<div class="champs">
						<div id="champ3">
							<label for="champ-largeur">Largeur</label>
							<input id="champ-largeur" type="number" min="0" value="100">
						</div>
						<div id="champ4">
							<label for="champ-unite-largeur">Unité</label>
							<select id="champ-unite-largeur">
								<option value="px">px</option>
								<option value="%" selected>%</option>
							</select>
						</div>
						<div id="champ5">
							<label for="champ-hauteur">Hauteur</label>
							<input id="champ-hauteur" type="number" min="0" value="500">
						</div>
						<div id="champ6">
							<label for="champ-unite-hauteur">Unité</label>
							<select for="champ-unite-hauteur">
								<option value="px" selected>px</option>
								<option value="%">%</option>
							</select>
						</div>
					</div>
					<div class="champs">
						<div id="champ7">
							<label for="champ-bordure">Bordure</label>
							<select id="champ-bordure">
								<option value="oui">Oui</option>
								<option value="non" selected>Non</option>
							</select>
						</div>
						<div id="champ8">
							<label for="champ-taille">Taille (px)</label>
							<input id="champ-taille" type="number" min="0" value="1">
						</div>
					</div>
				</div>
				<div class="actions">
					<span id="previsualiser" class="bouton" role="button" tabindex="0" onclick="previsualiser()">Prévisualiser</span>
					<span id="copier" class="bouton" role="button" tabindex="0" onclick="copier()">Copier le code</span>
				</div>
				<div id="code"><textarea></textarea></div>
				<div id="iframe"></div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="../clipboard.js"></script>
        <script>
			let lien = ''
			let nom = 'Mon iframe'
			let largeur = 100
			let hauteur = 500
			let uniteLargeur = '%'
			let uniteHauteur = 'px'
			let bordure = 'non'
			let tailleBordure = 1
			let iframe = ''

			document.querySelector('#previsualiser').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					previsualiser()
				}
			})

			document.querySelector('#copier').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					copier()
				}
			})
			
			function previsualiser () {
				lien = document.querySelector('#champ1 input').value
				nom = document.querySelector('#champ2 input').value
				largeur = document.querySelector('#champ3 input').value
				uniteLargeur = document.querySelector('#champ4 select').value
				hauteur = document.querySelector('#champ5 input').value
				uniteHauteur = document.querySelector('#champ6 select').value
				bordure = document.querySelector('#champ7 select').value
				tailleBordure = document.querySelector('#champ8 input').value
				if (bordure === 'non') {
					iframe = '<iframe src="' + lien + '" name="' + nom + '" title="' + nom + '" style="width: ' + largeur + uniteLargeur + '; height: ' + hauteur + uniteHauteur + '; border: none;" allowfullscreen></iframe>'
				} else {
					iframe = '<iframe src="' + lien + '" name="' + nom + '" title="' + nom + '" style="width: ' + largeur + uniteLargeur + '; height: ' + hauteur + uniteHauteur + '; border: ' + tailleBordure + 'px solid #ddd;" allowfullscreen></iframe>'
				}
				document.querySelector('#code').style.display = 'block'
				document.querySelector('#code textarea').value = iframe
				document.querySelector('#copier').style.display = 'inline-block'
				document.querySelector('#iframe').style.display = 'block'
				document.querySelector('#iframe').innerHTML = iframe
            }
			
			function copier () {
				const code = document.querySelector('#code textarea').value
				const clipboard = new ClipboardJS('#copier', {
					text: function () {
						return code
					}
				})
				clipboard.on('success', function () {
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Code copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
			}

			window.addEventListener('load', function () {
				if (bordure === 'oui') {
					document.querySelector('#champ8').style.display = 'block'
				}
				document.querySelector('#champ7 select').addEventListener('change', function (event) {
					if (event.target.value === 'oui') {
						document.querySelector('#champ8').style.display = 'block'
					} else {
						document.querySelector('#champ8').style.display = 'none'
					}
				})
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
			}
        </script>
    </body>
</html>
