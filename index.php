<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Digitools est une boîte à outils pour la classe en présence ou à distance.">
		<meta name="robots" content="index, no-follow" />
		<meta name="theme-color" content="#00ced1">
		<meta name="format-detection" content="telephone=no">
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="HandheldFriendly" content="true">
		<meta property="og:title" content="Digitools by La Digitale">
		<meta property="og:description" content="Digitools est une boîte à outils pour la classe en présence ou à distance.">
		<meta property="og:type" content="website" />
		<meta property="og:url" content="https://ladigitale.dev/digitools" />
		<meta property="og:image" content="https://ladigitale.dev/digitools/digitools.jpg" />
		<meta property="og:locale" content="fr_FR" />
        <title>Digitools by La Digitale</title>
		<link rel="stylesheet" href="./destyle.css">
		<link rel="stylesheet" href="./commun.css?v=1734033413663" type="text/css">
        <link rel="stylesheet" href="./style.css?v=1734033413663" type="text/css">
		<link rel="apple-touch-icon" href="./apple-touch-icon.png">
		<link rel="icon" href="./favicon.png">
    </head>
    <body>
		<header>Digitools by La Digitale</header>
        <div class="conteneur">
			<a href="./bataille-5/">
				<span>Bataille navale (5x5)</span>
			</a>
			<a href="./bataille-7/">
				<span>Bataille navale (7x7)</span>
			</a>
			<a href="./cartes/">
				<span>Cartes</span>
			</a>
			<a href="./chrono/">
				<span>Chronomètre</span>
			</a>
			<a href="./classement/">
				<span>Classement</span>
			</a>
			<a href="./coffre/">
				<span>Coffre-fort</span>
			</a>
			<a href="./rebours/">
				<span>Compte à rebours</span>
			</a>
			<a href="./des/">
				<span>Dés</span>
			</a>
			<a href="./feu/">
				<span>Feu tricolore</span>
			</a>
			<a href="./generateur-histoire/">
				<span>Générateur d'histoire</span>
			</a>
			<a href="./generateur-iframe/">
				<span>Générateur d'iframe</span>
			</a>
			<a href="./generateur-texte/">
				<span>Générateur de texte</span>
			</a>
			<a href="./groupes/">
				<span>Groupes</span>
			</a>
			<a href="./retroactions/">
				<span>Rétroactions</span>
			</a>
			<a href="./revelateur-image/">
				<span>Révélateur d'image</span>
			</a>
			<a href="./roue/">
				<span>Roue de la fortune</span>
			</a>
			<a href="./tableau-sons/">
				<span>Tableau de sons</span>
			</a>
			<a href="./tirage-image/">
				<span>Tirage au sort (image)</span>
			</a>
			<a href="./tirage-nombre/">
				<span>Tirage au sort (nombre)</span>
			</a>
			<a href="./tirage-texte/">
				<span>Tirage au sort (texte)</span>
			</a>
        </div>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p class="soutien"><a href="https://opencollective.com/ladigitale" target="_blank" rel="noreferrer">Je souhaite apporter mon soutien ❤️.</a></p>
			<p class="credits"><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a> - <a href="https://codeberg.org/ladigitale/digitools" target="_blank" rel="noreferrer">Code source</a> - <span class="hub" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#001d1d" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M4 8h4V4H4v4zm6 12h4v-4h-4v4zm-6 0h4v-4H4v4zm0-6h4v-4H4v4zm6 0h4v-4h-4v4zm6-10v4h4V4h-4zm-6 4h4V4h-4v4zm6 6h4v-4h-4v4zm0 6h4v-4h-4v4z"/></svg></span></p>
		</footer>
		
		<div id="hub" tabindex="-1">
			<span role="button" tabindex="-1"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#fff" width="36px" height="36px"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/></svg></span>
			<iframe src="https://ladigitale.dev/hub.html" title="Le Hub by La Digitale"></iframe>
		</div>
		
		<script>
			document.querySelector('.hub').addEventListener('keydown', function (event) {
				if (event.key === 'Enter') {
					document.querySelector('#hub').classList.add('ouvert');
					document.querySelector('#hub span').setAttribute('tabindex', '0');
					document.querySelector('#hub span').focus();
				}
			});

			document.querySelector('#hub span').addEventListener('click', function () {
				document.querySelector('#hub').classList.remove('ouvert');
				document.querySelector('#hub span').setAttribute('tabindex', '-1');
				document.querySelector('.hub').focus();
			});

			document.querySelector('#hub span').addEventListener('keydown', function (event) {
				if (event.key === 'Enter') {
					document.querySelector('#hub').classList.remove('ouvert');
					document.querySelector('#hub span').setAttribute('tabindex', '-1');
					document.querySelector('.hub').focus();
				}
			});
		</script>
    </body>
</html>
