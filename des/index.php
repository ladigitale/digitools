<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour afficher des dés.">
        <title>Dés - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
		<link rel="stylesheet" href="dice.css" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Dés</span>
		</header>
		<main>
			<div class="conteneur conteneur-edition">
				<div id="champ">
					<label for="champ-des">Nombre de dés</label>
					<input id="champ-des" type="number" value="1" min="1" max="6">
				</div>
				<div class="actions">
					<span id="demarrer" class="bouton" role="button" tabindex="0" onclick="demarrer()">Démarrer</span>
				</div>
			</div>
			<div class="conteneur conteneur-des">
				<div id="des"></div>
				<div class="actions">
					<span id="jeter" class="bouton" role="button" tabindex="0" onclick="jeter()">Jeter</span>
					<span id="quitter" class="bouton" role="button" tabindex="0" onclick="quitter()">Quitter</span>
				</div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>

        <script>
			let des = 1
			const audio = new Audio('des.mp3')

			document.querySelector('#demarrer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					demarrer()
				}
			})

			document.querySelector('#jeter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					jeter()
				}
			})

			document.querySelector('#quitter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					quitter()
				}
			})

            function lireSon () {
                audio.pause()
                audio.currentTime = 0
                audio.play()
            }
			
			function demarrer () {
				des = document.querySelector('#champ input').value
				let html = ''
				for (let i = 0; i < des; i++) {
					html += '<span class="dice dice-1"></span>'
				}
				document.querySelector('#des').innerHTML = html
				document.querySelector('.conteneur-edition').style.display = 'none'
				document.querySelector('.conteneur-des').style.display = 'block'
			}

			function jeter () {
				const resultats = []
				for (let i = 0; i < des; i++) {
					const resultat = Math.floor(Math.random() * (6 - 1 + 1) + 1)
					resultats.push(resultat)
				}
				let html = ''
				resultats.forEach(function (de) {
					html += '<span class="dice dice-' + de + '"></span>'
				})
				document.querySelector('#des').innerHTML = html
				lireSon()
			}
			
			function quitter () {
				document.querySelector('.conteneur-edition').style.display = 'block'
				document.querySelector('.conteneur-des').style.display = 'none'
			}

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				document.querySelector('.conteneur-des').style.padding = '30px 20px'
			}
        </script>
    </body>
</html>
