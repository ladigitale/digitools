<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour diffuser des sons.">
        <title>Rétroactions - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Rétroactions</span>
		</header>
		<main>
			<div class="conteneur">
				<div id="retroactions">
					<span id="confettis" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('applaudissements', 'confettis')" style="background-image: url(./confettis.svg)"></span>
					<span id="applaudissements" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('applaudissements', 'applaudissements')" style="background-image: url(./applaudissements.svg)"></span>
					<span id="victoire" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('victoire', 'victoire')" style="background-image: url(./victoire.svg)"></span>
					<span id="dommage" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('dommage', 'dommage')" style="background-image: url(./dommage.svg)"></span>
					<span id="vrai" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('vrai', 'vrai')" style="background-image: url(./vrai.svg)"></span>
					<span id="faux" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('faux', 'faux')" style="background-image: url(./faux.svg)"></span>
					<span id="pouce-haut" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('vrai', 'pouce-haut')" style="background-image: url(./pouce-haut.svg)"></span>
					<span id="pouce-bas" class="bouton" role="button" tabindex="0" onclick="ouvrirCarte('faux', 'pouce-bas')" style="background-image: url(./pouce-bas.svg)"></span>
				</div>
			</div>
			<div id="carte-confettis" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./confettis.svg" alt="confettis"></span>
			</div>
			<div id="carte-applaudissements" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./applaudissements.svg" alt="applaudissements"></span>
			</div>
			<div id="carte-victoire" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./victoire.svg" alt="victoire"></span>
			</div>
			<div id="carte-dommage" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./dommage.svg" alt="dommage"></span>
			</div>
			<div id="carte-vrai" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./vrai.svg" alt="vrai"></span>
			</div>
			<div id="carte-faux" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./faux.svg" alt="faux"></span>
			</div>
			<div id="carte-pouce-haut" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./pouce-haut.svg" alt="pouce-haut"></span>
			</div>
			<div id="carte-pouce-bas" class="conteneur-carte" onclick="fermerCarte()">
				<span class="carte"><img role="button" tabindex="0" src="./pouce-bas.svg" alt="pouce-bas"></span>
			</div>
			<audio id="audio-applaudissements" preload="auto"><source src="./applaudissements.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-victoire" preload="auto"><source src="./victoire.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-dommage" preload="auto"><source src="./dommage.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-vrai" preload="auto"><source src="./vrai.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-faux" preload="auto"><source src="./faux.mp3" type="audio/mpeg" style="display: none;"></audio>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="confetti.js"></script>
        <script>
			let audio = ''

			document.querySelector('#confettis').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('applaudissements', 'confettis')
				}
			})

			document.querySelector('#applaudissements').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('applaudissements', 'applaudissements')
				}
			})

			document.querySelector('#victoire').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('victoire', 'victoire')
				}
			})

			document.querySelector('#dommage').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('dommage', 'dommage')
				}
			})

			document.querySelector('#vrai').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('vrai', 'vrai')
				}
			})

			document.querySelector('#faux').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('faux', 'faux')
				}
			})

			document.querySelector('#pouce-haut').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('vrai', 'pouce-haut')
				}
			})

			document.querySelector('#pouce-bas').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					ouvrirCarte('faux', 'pouce-bas')
				}
			})
			
			function ouvrirCarte (son, carte) {
				document.querySelector('#carte-' + carte).classList.add('ouvert')
				setTimeout(() => {
					document.querySelector('#carte-' + carte + ' img').focus()
				}, 0)
				if (audio !== '') {
					audio.pause()
					audio.currentTime = 0
				}
				audio = document.querySelector('#audio-' + son)
				audio.play()
				if (carte === 'confettis' || carte === 'applaudissements' || carte === 'victoire') {
					lancerConfettis()
				}
			}

			function lancerConfettis () {
				confetti({ angle: 300, spread: 55, particleCount: 150, origin: { x: 0, y: -0.2 }, zIndex: 10010 })
				confetti({ angle: 240, spread: 55, particleCount: 150, origin: { x: 1, y: -0.2 }, zIndex: 10010 })
				confetti({ angle: 270, spread: 70, particleCount: 150, origin: { x: 0.5, y: -0.2 }, zIndex: 10010 })
			}

			const cartes = document.querySelectorAll('.carte img')
			cartes.forEach(function (carte) {
				carte.addEventListener('keydown', function (event) {
					if (event.key === 'Enter') {
						fermerCarte()
					}
				})
			})

			function fermerCarte () {
				if (audio !== '') {
					audio.pause()
					audio.currentTime = 0
				}
				if (document.querySelector('.conteneur-carte.ouvert')) {
					document.querySelector('.conteneur-carte.ouvert').classList.remove('ouvert')
				}
			}
        </script>
    </body>
</html>
