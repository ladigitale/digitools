<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour diffuser des sons.">
        <title>Tableau de sons - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Tableau de sons</span>
		</header>
		<main>
			<div class="conteneur">
				<div id="sons">
					<span id="applaudissements" role="button" tabindex="0" onclick="lireAudio('applaudissements')">Applaudissements</span>
					<span id="tada" role="button" tabindex="0" onclick="lireAudio('tada')">Tada</span>
					<span id="victoire" role="button" tabindex="0" onclick="lireAudio('victoire')">Victoire</span>
					<span id="dommage" role="button" tabindex="0" onclick="lireAudio('dommage')">Dommage</span>
					<span id="vrai" role="button" tabindex="0" onclick="lireAudio('vrai')">Vrai</span>
					<span id="faux" role="button" tabindex="0" onclick="lireAudio('faux')">Faux</span>
				</div>
			</div>
			<audio id="audio-applaudissements" preload="auto"><source src="./applaudissements.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-tada" preload="auto"><source src="./tada.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-victoire" preload="auto"><source src="./victoire.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-dommage" preload="auto"><source src="./dommage.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-vrai" preload="auto"><source src="./vrai.mp3" type="audio/mpeg" style="display: none;"></audio>
			<audio id="audio-faux" preload="auto"><source src="./faux.mp3" type="audio/mpeg" style="display: none;"></audio>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
        <script>
			let audio = ''

			const sons = document.querySelectorAll('#sons span')
			sons.forEach(function (son) {
				son.addEventListener('keydown', function (event) {
					if (event.key === 'Enter') {
						lireAudio(son.id)
					}
				})
			})

			function lireAudio (son) {
				if (audio !== '') {
					audio.pause()
					audio.currentTime = 0
				}
				audio = document.querySelector('#audio-' + son)
				audio.play()
			}
        </script>
    </body>
</html>
