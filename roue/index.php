<?php

$url = '';
$parametres = '';
$token = '';
$admin = 0;
$vues = 0;

if (!empty($_GET['p'])) {
	require 'db.php';
	if (!empty($_GET['t'])) {
		$token = $_GET['t'];
	}
	$url = $_GET['p'];
	$stmt = $db->prepare('SELECT token, parametres, vues FROM digitools WHERE url = :url');
	if ($stmt->execute(array('url' => $url))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			$db = null;
			return false;
		}
		if ($resultat[0]['parametres'] && $resultat[0]['parametres'] !== '') {
			$parametres = rawurlencode($resultat[0]['parametres']);
		} else {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] === $token) {
			$admin = 1;
		} else if ($resultat[0]['token'] && $resultat[0]['token'] !== '' && $token !== '' && $resultat[0]['token'] !== $token) {
			header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
		}
		$date = date('Y-m-d H:i:s');
		if ($resultat[0]['vues'] !== '') {
			$vues = intval($resultat[0]['vues']);
		}
		if ($admin === 0) {
			$vues = $vues + 1;
		}
		$stmt = $db->prepare('UPDATE digitools SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
		$stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $url));
	}
	$db = null;
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour créer une roue de la fortune.">
        <title>Roue de la fortune - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Roue de la fortune</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Indiquez dans le champ ci-dessous les éléments de la roue, séparés par une virgule (maximum 24 éléments). <u>Exemple</u>&nbsp;: 100, 200, 300, 400.</p>
			</div>
			<div class="conteneur conteneur-options">
				<label for="options">Liste des éléments</label>
				<textarea id="options"></textarea>
				<div class="actions">
					<span id="recommencer" class="bouton" role="button" tabindex="0" onclick="recommencer()">Recommencer</span>
					<span id="creer" class="bouton" role="button" tabindex="0" onclick="creer('enregistrer')">Valider</span>
				</div>
			</div>
			<div class="conteneur conteneur-roue">
				<div id="roue">
					<canvas id="canvas" width="434" height="434" data-responsiveScaleHeight="true" data-responsiveMargin="40"></canvas>
				</div>
				<div class="actions">
					<span id="lancer" class="bouton" role="button" tabindex="0" onclick="lancer()">Lancer</span>
					<span id="quitter" class="bouton" role="button" tabindex="0" onclick="quitter()">Quitter</span>
					<span id="stopper" class="bouton" role="button" tabindex="0" onclick="stopper()">Stopper</span>
				</div>
				<div id="resultat"></div>
			</div>
		</main>
		<footer>
			<?php if ($vues > 1) { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie. - <?php echo $vues; ?> vues</p>
			<?php } else { ?>
				<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<?php } ?>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>
		
		<script type="text/javascript" src="winwheel.js"></script>
        <script type="text/javascript" src="tweenmax.js"></script>
		<script type="text/javascript" src="../clipboard.js"></script>
        <script>
            let roue = ''
			let liste = ''
			let url = ''
			let token = ''
			const hote = window.location.href.split('?')[0]
            const audio = new Audio('tick.mp3')

			document.querySelector('#recommencer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					recommencer()
				}
			})

			document.querySelector('#creer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					creer('enregistrer')
				}
			})

			document.querySelector('#lancer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					lancer()
				}
			})

			document.querySelector('#quitter').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					quitter()
				}
			})

			document.querySelector('#stopper').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					stopper()
				}
			})

            function lireSon () {
                audio.pause()
                audio.currentTime = 0
                audio.play()
            }

            function afficherResultat (valeur) {
				let resultat = valeur.text
				if (valeur.hasOwnProperty('original')) {
					resultat = valeur.original
				}
				document.querySelector('#lancer').style.display = 'inline-block'
				if (document.querySelector('#quitter')) {
					document.querySelector('#quitter').style.display = 'inline-block'
				}
				document.querySelector('#stopper').style.display = 'none'
				document.querySelector('#resultat').textContent = resultat
				document.querySelector('#resultat').style.display = 'flex'
            }
			
			function creer (action) {
				if (action === 'lire') {
					liste = document.querySelector('#options').value
					if (liste !== '') {
						const options = []
						liste = liste.split(',')
						const items = liste.length
						if (items < 25) {
							const longueurs = []
							const radius = 212
							liste.forEach(function (item, index) {
								const couleur = definirCouleur(items, index)
								let texte = item.trim()
								const texteOriginal = texte
								if (texte.length > 19) {
									texte = texte.substring(0, 18) + '…'
								}
								options.push({ 'fillStyle': couleur, 'text': texte, 'original': texteOriginal })
								longueurs.push(texte.length)
							})
							const longueur = Math.max(...longueurs)
							let taille = 20
							if (longueur >= 15) {
								taille = 20
							} else if (longueur >= 11 && longueur <= 14) {
								taille = 26
							} else if (longueur >= 6 && longueur <= 10) {
								taille = 32
							} else if (longueur <= 5) {
								taille = 40
							}
							if (options.length > 12) {
								taille = Math.floor(taille / 1.5)
							}
							roue = new Winwheel({
								'responsive': true,
								'numSegments': options.length,
								'outerRadius': radius,
								'textOrientation': 'horizontal',
								'textFontFamily': 'sans-serif',
								'textDirection': 'reversed',
								'textAlignment': 'outer',
								'textFontSize': taille,
								'segments': options,
								'animation': {
									'type': 'spinToStop',
									'duration': 7,
									'spins': options.length,
									'callbackFinished': afficherResultat,
									'callbackSound': lireSon,
									'soundTrigger': 'pin'
								},
								'pins': {
									'number': options.length * 2
								}
							})
							roue.draw()
							redimensionner()
							document.querySelector('#lancer').style.display = 'inline-block'
							if (document.querySelector('#quitter')) {
								document.querySelector('#quitter').style.display = 'inline-block'
							}
							document.querySelector('.conteneur-roue').style.display = 'block'
							document.querySelector('.conteneur-consigne').style.display = 'none'
							document.querySelector('.conteneur-options').style.display = 'none'
							document.querySelector('#stopper').style.display = 'none'
						} else {
							alert('Le nombre maximum d\'éléments est 24.')
						}
					}
				} else {
					liste = document.querySelector('#options').value
					if (liste !== '') {
						const xhr = new XMLHttpRequest()
						xhr.onload = function () {
							if (xhr.readyState === xhr.DONE && xhr.status === 200) {
								if (xhr.responseText !== 'erreur') {
									const donnees = JSON.parse(xhr.responseText)
									url = donnees.url
									token = donnees.token
									window.location = hote + '?p=' + url + '&t=' + token
								} else {
									alert('Erreur de communication avec le serveur.')
								}
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						}
						xhr.open('POST', hote + 'enregistrer.php', true)
						xhr.setRequestHeader('Content-type', 'application/json')
						xhr.send(JSON.stringify({ url: url, token: token, parametres: liste }))
					}
				} 
			}

            function lancer () {
				roue.stopAnimation(false)
                roue.rotationAngle = 0
                roue.draw()
                roue.animation.spins = 7
                roue.startAnimation()
				document.querySelector('#stopper').style.display = 'inline-block'
				document.querySelector('#lancer').style.display = 'none'
				if (document.querySelector('#quitter')) {
					document.querySelector('#quitter').style.display = 'none'
				}
				document.querySelector('#resultat').style.display = 'none'
            }
			
			function stopper () {
				roue.stopAnimation(true)
			}

            function quitter () {
                roue.stopAnimation(false)
                roue.rotationAngle = 0
                roue.draw()
				document.querySelector('#creer').textContent = 'Modifier'
				document.querySelector('#recommencer').style.display = 'inline-block'
				document.querySelector('.conteneur-consigne').style.display = 'block'
				document.querySelector('.conteneur-options').style.display = 'block'
				document.querySelector('.conteneur-roue').style.display = 'none'
				if (document.querySelector('#quitter')) {
					document.querySelector('#quitter').style.display = 'none'
				}
				document.querySelector('#lancer').style.display = 'none'
				document.querySelector('#resultat').style.display = 'none'
            }
			
			function definirCouleur (items, index) {
				let couleurs = ['#eae56f', '#89f26e', '#7de6ef', '#e7706f']
				if (items % 4 === 0) {
					if (index < 4) {
						return couleurs[index]
					} else if (index < 8) {
						return couleurs[index - 4]
					} else if (index < 12) {
						return couleurs[index - 8]
					} else if (index < 16) {
						return couleurs[index - 12]
					} else if (index < 20) {
						return couleurs[index - 16]
					} else {
						return couleurs[index - 20]
					}
				} else if (items % 3 === 0) {
					if (index < 3) {
						return couleurs[index]
					} else if (index < 6) {
						return couleurs[index - 3]
					} else if (index < 9) {
						return couleurs[index - 6]
					} else if (index < 12) {
						return couleurs[index - 9]
					} else if (index < 15) {
						return couleurs[index - 12]
					} else if (index < 18) {
						return couleurs[index - 15]
					} else if (index < 21) {
						return couleurs[index - 18]
					} else {
						return couleurs[index - 21]
					}
				} else {
					couleurs = ['#fc65bc', '#d96df3', '#e07cd9', '#f38785', '#f89955', '#f89955', '#fadc0c', '#fff22f', '#f7ee27', '#e0e855', '#c1d10c', '#76d376', '#4fbcac', '#00bbb7', '#9dcbd8', '#a9bcf3', '#87a1f8', '#8295ce', '#d4c7c1', '#d3cecb', '#dddddd', '#e7e7e7', '#f7f7f7']
					return couleurs[index]
				}
			}
			
			function recommencer () {
				window.location = hote
			}
			
			function genererLiens (url, token) {
				const lienAdmin = hote + '?p=' + url + '&t=' + token
				const lien = hote + '?p=' + url
				const element = document.createElement('div')
				element.id = 'liens'
				element.innerHTML = '<label>Lien d\'administration</label><input type="text" id="lien-admin" value="' + lienAdmin + '" disabled><span id="copier-admin" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><label>Lien de partage</label><input type="text" id="lien" value="' + lien + '" disabled><span id="copier" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24px" height="24px"><path d="M0 0h24v24H0z" fill="none"></path><path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></svg></span><div id="supprimer"><span class="bouton" role="button" tabindex="0" onclick="supprimer()">Supprimer</span></div>'
				document.querySelector('footer').insertAdjacentElement('beforebegin', element)

				document.querySelector('#copier-admin').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier-admin').click()
					}
				})

				document.querySelector('#copier').addEventListener('keydown', function (e) {
					if (e.key === 'Enter') {
						document.querySelector('#copier').click()
					}
				})

				const clipboardAdmin = new ClipboardJS('#copier-admin', {
					text: function () {
						return lienAdmin
					}
				})
				clipboardAdmin.on('success', function () {
					document.querySelector('#copier-admin').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien d\'administration copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
				const clipboardLien = new ClipboardJS('#copier', {
					text: function () {
						return lien
					}
				})
				clipboardLien.on('success', function () {
					document.querySelector('#copier').focus()
					const element = document.createElement('div')
					const id = 'notification_' + Date.now().toString(36) + Math.random().toString(36).substring(2)
					element.id = id
					element.textContent = 'Lien de partage copié dans le presse-papier.'
					element.classList.add('notification')
					document.querySelector('main').appendChild(element)
					setTimeout(function () {
						element.parentNode.removeChild(element)
					}, 2500)
				})
			}

			function redimensionner () {
				winwheelResize()
				const largeur = document.querySelector('#canvas').width + 4
				const hauteur = largeur * (582 / 438)
				document.querySelector('#roue').style.width = largeur + 'px'
				document.querySelector('#roue').style.height = hauteur + 'px'
			}

			function supprimer () {
				if (confirm('Souhaitez-vous vraiment supprimer ce contenu ?')) {
					const xhr = new XMLHttpRequest()
					xhr.onload = function () {
						if (xhr.readyState === xhr.DONE && xhr.status === 200) {
							if (xhr.responseText !== 'erreur') {
								window.location = hote
							} else {
								alert('Erreur de communication avec le serveur.')
							}
						} else {
							alert('Erreur de communication avec le serveur.')
						}
					}
					xhr.open('POST', hote + 'supprimer.php', true)
					xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
					xhr.send('url=' + url + '&token=' + token)
				} 
			}
			
			window.addEventListener('load', function () {
				if ('<?php echo $url; ?>' !== '' && '<?php echo $parametres; ?>' !== '') {
					url = '<?php echo $url; ?>'
					document.querySelector('#options').value = decodeURIComponent('<?php echo $parametres; ?>')
					creer('lire')
				} else {
					document.querySelector('#recommencer').style.display = 'none'
				}
				if (parseInt('<?php echo $admin; ?>') === 1) {
					token = '<?php echo $token; ?>'
					if (window === window.parent) {
						genererLiens(url, token)
					}
				} else {
					document.querySelector('#quitter').parentNode.removeChild(document.querySelector('#quitter'))
				}
				document.body.style.display = 'block'
			})

			window.addEventListener('resize', function () {
				redimensionner()
			})

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
				if (document.querySelector('.conteneur-roue')) {
					document.querySelector('.conteneur-roue').style.marginTop = '0'
					document.querySelector('.conteneur-roue').style.border = 'none'
				}
			}
        </script>
    </body>
</html>
