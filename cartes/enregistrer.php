<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['parametres'])) {
	require 'db.php';
	$parametres = $_POST['parametres'];
	if ($_POST['url'] === '') {
		$url = uniqid('', false);
		$token = substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'), 0, 10);
		$date = date('Y-m-d H:i:s');
		$vues = 0;
		$stmt = $db->prepare('INSERT INTO digitools (url, token, parametres, date, vues, derniere_visite) VALUES (:url, :token, :parametres, :date, :vues, :derniere_visite)');
		if ($stmt->execute(array('url' => $url, 'token' => $token, 'parametres' => $parametres, 'date' => $date, 'vues' => $vues, 'derniere_visite' => $date))) {
			echo json_encode(array('url' => $url, 'token' =>  $token));
		} else {
			echo 'erreur';
		}
	} else {
		$url = $_POST['url'];
		$token = $_POST['token'];
		$stmt = $db->prepare('SELECT token FROM digitools WHERE url = :url');
		if ($stmt->execute(array('url' => $url))) {
			$resultat = $stmt->fetchAll();
			if (!$resultat) {
				header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			} else if ($resultat[0]['token'] === $token) {
				$stmt = $db->prepare('UPDATE digitools SET parametres = :parametres WHERE url = :url');
				if ($stmt->execute(array('parametres' => $parametres, 'url' => $url))) {
					echo json_encode(array('url' => $url, 'token' =>  $token));
				} else {
					echo 'erreur';
				}
			} else {
				header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
			}
		}
	}
	$db = null;
	exit();
} else {
	header('Location: ' . explode('?', $_SERVER['REQUEST_URI'])[0]);
}

?>
