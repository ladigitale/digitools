<!DOCTYPE html>
<html lang="fr">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1.0, maximum-scale=5.0">
		<meta name="description" content="Un outil de la suite Digitools pour jouer à la bataille navale.">
        <title>Bataille navale (5x5) - Digitools by La Digitale</title>
		<link rel="stylesheet" href="../destyle.css" type="text/css">
		<link rel="stylesheet" href="../commun.css?v=1672584771480" type="text/css">
        <link rel="stylesheet" href="style.css" type="text/css">
		<link rel="icon" type="image/png" href="../favicon.png">
    </head>
    <body>
		<header>
			<a href="../" title="Retour" role="button" tabindex="0"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="32px" height="32px" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"><g fill="#ffffff"><path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/><path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/><path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/></g><rect x="0" y="0" width="16" height="16" fill="rgba(0, 0, 0, 0)" /></svg></a>
			<span>Bataille navale (5x5)</span>
		</header>
		<main>
			<div class="conteneur conteneur-consigne">
				<p>Ce module propose des mécanismes simples pour mettre en place des déclinaisons du jeu <i>Bataille navale</i> en contexte éducatif.</p>
				<p class="petit"><a href="./bataille-du-feminin.pdf" target="_blank">Voir un exemple de règles de jeu</a></p>
			</div>
			<div class="conteneur conteneur-edition">
				<div class="conteneur-grille">
					<label>Mes navires</label>
					<div id="grille1" class="grille">
						<div class="case"></div>
						<div class="case">A</div>
						<div class="case">B</div>
						<div class="case">C</div>
						<div class="case">D</div>
						<div class="case">E</div>
						<div class="case">1</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">2</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">3</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">4</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">5</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
					</div>
					<div class="actions">
						<span id="creer" class="bouton" role="button" tabindex="0" onclick="valider()">Valider</span>
					</div>
				</div>
				<div class="conteneur-grille">
					<label>Ses navires</label>
					<div id="grille2" class="grille">
						<div class="case"></div>
						<div class="case">A</div>
						<div class="case">B</div>
						<div class="case">C</div>
						<div class="case">D</div>
						<div class="case">E</div>
						<div class="case">1</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">2</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">3</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">4</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case">5</div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
						<div class="case active" role="button" tabindex="0"></div>
					</div>
				</div>
			</div>
		</main>
		<footer>
			<p>Cette page 100% cousue main ne collecte aucune donnée personnelle et n'utilise aucun cookie.</p>
			<p><?php echo date("Y"); ?> - <a href="https://ladigitale.dev" target="_blank" rel="noreferrer">La Digitale</a></p>
			<a href="../">Retour à la liste des outils</a>
		</footer>

        <script>
			let edition = true

			const casesGrille1 = document.querySelectorAll('#grille1 .case.active')
			for (let i = 0; i < casesGrille1.length; i++) {
				casesGrille1[i].addEventListener('click', function () {
					if (edition === true && (casesGrille1[i].style.background === '' || convertirCouleur(casesGrille1[i].style.background) !== '#ffff00')) {
						casesGrille1[i].style.background = '#ffff00'
					} else if (edition === true) {
						casesGrille1[i].style.background = '#ffffff'
					}
				})

				casesGrille1[i].addEventListener('keydown', function (e) {
					if (e.key === 'Enter' && (edition === true && (casesGrille1[i].style.background === '' || convertirCouleur(casesGrille1[i].style.background) !== '#ffff00'))) {
						casesGrille1[i].style.background = '#ffff00'
					} else if (e.key === 'Enter' && edition === true) {
						casesGrille1[i].style.background = '#ffffff'
					}
				})
			}

			const casesGrille2 = document.querySelectorAll('#grille2 .case.active')
			for (let i = 0; i < casesGrille2.length; i++) {
				casesGrille2[i].addEventListener('click', function () {
					if (casesGrille2[i].style.background === '' || convertirCouleur(casesGrille2[i].style.background) === '#ffffff') {
						casesGrille2[i].style.background = '#08fe07'
					} else if (convertirCouleur(casesGrille2[i].style.background) === '#08fe07') {
						casesGrille2[i].style.background = '#ee3926'
					} else if (convertirCouleur(casesGrille2[i].style.background) === '#ee3926') {
						casesGrille2[i].style.background = '#ffffff'
					}
				})

				casesGrille2[i].addEventListener('keydown', function (e) {
					if (e.key === 'Enter' && (casesGrille2[i].style.background === '' || convertirCouleur(casesGrille2[i].style.background) === '#ffffff')) {
						casesGrille2[i].style.background = '#08fe07'
					} else if (e.key === 'Enter' && convertirCouleur(casesGrille2[i].style.background) === '#08fe07') {
						casesGrille2[i].style.background = '#ee3926'
					} else if (e.key === 'Enter' && convertirCouleur(casesGrille2[i].style.background) === '#ee3926') {
						casesGrille2[i].style.background = '#ffffff'
					}
				})
			}

			document.querySelector('#creer').addEventListener('keydown', function (e) {
				if (e.key === 'Enter') {
					valider()
				}
			})

			function valider () {
				edition = false
				document.querySelector('#creer').remove()
			}

			function convertirCouleur (couleur) {
				const parties = couleur.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/)
				delete(parties[0])
				for (let i = 1; i <= 3; ++i) {
					parties[i] = parseInt(parties[i]).toString(16)
					if (parties[i].length == 1) {
						parties[i] = '0' + parties[i]
					}
				}
				return '#' + parties.join('')
			}

			if (window !== window.parent) {
				document.querySelector('header').style.display = 'none'
				document.querySelector('footer').style.display = 'none'
			}
        </script>
    </body>
</html>
